#include <iostream>
#include <afx.h>

using namespace std;

class CGraphics : public CObject
{
	DECLARE_DYNAMIC(CGraphics)
};

class CLine : public CGraphics
{
	DECLARE_DYNAMIC(CLine)
};

class CRectangle : public CGraphics
{
	DECLARE_DYNAMIC(CRectangle)
};

IMPLEMENT_DYNAMIC(CRectangle, CGraphics)
IMPLEMENT_DYNAMIC(CLine, CGraphics)
IMPLEMENT_DYNAMIC(CGraphics, CObject)

int main()
{
	CRectangle *pRect = new CRectangle();
	CLine *pLine = new CLine();

	cout << "pRect->IsKindOf(RUNTIME_CLASS(CGraphics)): " << pRect->IsKindOf(RUNTIME_CLASS(CGraphics)) <<endl; 

	cout << "pLine->IsKindOf(RUNTIME_CLASS(CGraphics)): " << pLine->IsKindOf(RUNTIME_CLASS(CGraphics)) <<endl; 

	cout << "pLine->IsKindOf(RUNTIME_CLASS(CRectangle)): " << pLine->IsKindOf(RUNTIME_CLASS(CRectangle)) <<endl; 
}